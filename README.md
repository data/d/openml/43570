# OpenML dataset: AndrewNg-Machine-Learning-Tweets

https://www.openml.org/d/43570

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context

This Dataset, consist of Tweets regarding Machine Learning, posted by Prof.Andrew Ng,  Co-founder of Coursera, and an Adjunct Professor of Computer Science at Stanford University. His machine learning course is the MOOC that had led to the founding of Coursera!
He's one of my idol, in this huge field, Learned tonnes of new things from him, so decided to come up with this specific Dataset, which is dedicated towards Natural Language Processing.
Thank-you!
Content
The Dataset Consist of the 8 Columns, specifically Id, username, Name, Tweet, conversation_id, timezone ,time, and Polarity.
So, Basically Polarity column is the sentiment Analysed column, 
where,
1 = Neutral Tweets
2 = Slightly Negative Tweets
3 = Negative Tweets
4 = Slightly Positive Tweets
5 = Positive Tweets
Inspiration
This Dataset would be wonderful to work on Sentiment Analysis and NLP.
Thankyou so Much to work on this Dataset!
If you like this Dataset, 
Please do UpVote!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43570) of an [OpenML dataset](https://www.openml.org/d/43570). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43570/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43570/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43570/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

